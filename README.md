# Forum

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Instalação

Duplicar o arquivo `.env.example` e renomear para `.env`, informando os campos necessários, inicialmente, apenas o endereço da API.

Para integração com o back-end fornecido neste desafio, utilizar algo como:
```.env
REACT_APP_API=http://127.0.0.1:8000/api
```

Assim como a maioria dos projetos JS modernos, instala-se as dependências com o seguinte comando:

```
yarn
```

## Execução

### Desenvolvimento

Executar o seguinte comando:

```
yarn start
```
Roda a aplicação em modo de desenvolvimento.<br />
Abra [http://localhost:3000](http://localhost:3000) para visualizá-la no browser.

A página recarregará automaticamente, ao serem realizadas modificações. Você também verá quaisquer erros de lint no console.

### Produção

`yarn build`

Configura a aplicação para rodar em produção<br />
Gera um bundle em modo de produção e otimiza para a melhor performance. O _build_ é minificado e os nomes dos arquivos recebem _hashes_.
