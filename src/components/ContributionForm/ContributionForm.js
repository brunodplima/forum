import React, { useState } from 'react'

import './ContributionForm.sass'

const ContributionForm = (props) => {
  const [formState, setFormState] = useState({
    content: ''
  })

  const handleChange = (event) => {
    setFormState({ content: event.target.value })
  }

  const handleSubmit = (event) => {
    event.preventDefault()
    const { submitCallback } = props
    const { content } = formState
    submitCallback(content)
    setFormState({ content: '' })
  }

  const { title, subtitle } = props
  const { content } = formState

  return title && (
    <div className="contribution-form">
      <h2>{title}</h2>
      <p>{subtitle}</p>
      <form onSubmit={handleSubmit}>
        <textarea
          value={content}
          onChange={handleChange}
          rows="10"
          required
        />
        <input type="submit" value="Enviar" disabled={!content} />
      </form>
    </div>
  )
}

export default ContributionForm
