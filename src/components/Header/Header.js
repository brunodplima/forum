import React from 'react'

import './Header.sass'

const Header = () => (
  <header id="app-header">Fórum Q&A</header>
)

export default Header
