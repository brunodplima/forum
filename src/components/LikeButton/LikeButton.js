import React from 'react'
import heart from '../../assets/images/heart.svg'

import './LikeButton.sass'

const LikeButton = (props) => {
  const { likes, handleClick } = props

  return (
    <div
      className="like-button-wrapper"
      onClick={handleClick}
      onKeyPress={handleClick}
      role="button"
      tabIndex={0}
    >
      <img
        src={heart}
        className={`like-button ${likes > 0 ? 'filled' : ''}`}
        alt="Útil"
      />
      <span>{likes}</span>
    </div>
  )
}

export default LikeButton
