import React, { useState, useEffect } from 'react'
import { Link } from 'react-router-dom'
import api from '../../services/api'
import ContributionForm from '../../components/ContributionForm/ContributionForm'
import LikeButton from '../../components/LikeButton/LikeButton'

import './Homepage.sass'
import search from '../../shared/search'

const Homepage = () => {
  const [questionsState, setQuestionsState] = useState({
    questions: [],
    allQuestions: [],
  })
  const [sortState, setSortState] = useState('date')
  const [searchState, setSearchState] = useState('')
  const [unansweredOnlyState, setUnansweredOnlyState] = useState(false)

  const loadQuestions = async () => {
    const response = await api.get(`questions?orderBy=${sortState}`)
    setQuestionsState({ questions: response.data.data, allQuestions: response.data.data })
  }

  const handleSubmitNewQuestion = async (content) => {
    const { allQuestions } = questionsState
    const response = await api.post('questions', {
      question: content,
    })
    setQuestionsState({
      questions: [...allQuestions, response.data],
      allQuestions: [...allQuestions, response.data],
    })
    setSearchState('')
  }

  const handleLike = async (questionId) => {
    const response = await api.post(`questions/${questionId}/like`)
    const { questions } = questionsState
    const newQuestions = questions.map((question) => (
      question.id === questionId
        ? response.data
        : question
    ))

    setQuestionsState({ ...questionsState, questions: newQuestions })
  }

  const setSort = () => {
    setSortState(sortState === 'date' ? 'likes' : 'date')
  }

  const applyFilters = (searchTerm, unansweredOnly) => {
    setQuestionsState({
      ...questionsState,
      questions: search(searchTerm, questionsState.allQuestions)
        .filter((question) => (
          !unansweredOnly || question.numOfAnswers === 0
        ))
    })
  }

  const handleSearch = (event) => {
    const term = event.target.value
    applyFilters(term, unansweredOnlyState)
    setSearchState(term)
  }

  const toggleUnansweredOnly = (event) => {
    const isChecked = event.target.checked
    applyFilters(searchState, isChecked)
    setUnansweredOnlyState(isChecked)
  }

  useEffect(() => {
    loadQuestions()
  }, [sortState])

  const { questions } = questionsState

  return (
    <div className="question-list">
      <h1>Perguntas</h1>
      <div className="question-list-header">
        <button type="button" onClick={setSort}>Ordenado por <i>{sortState}</i></button>
        <label htmlFor="unansweredOnly">
          <input
            type="checkbox"
            onChange={toggleUnansweredOnly}
            id="unansweredOnly"
          />
          Apenas sem respostas
        </label>
        <input
          type="text"
          value={searchState}
          onChange={handleSearch}
          placeholder="Filtrar perguntas"
          // eslint-disable-next-line jsx-a11y/no-autofocus
          autoFocus
        />
      </div>
      {questions.map((question) => (
        <article key={question.id}>
          <div className="question-metadata">Pergunta #{question.id} [{question.createdAt}]</div>
          <div className="question">{question.question}</div>
          <p>Quantidade de respostas: {question.numOfAnswers}</p>
          <Link to={`/questions/${question.id}`}>Ver respostas</Link>
          <LikeButton
            likes={question.likes}
            handleClick={() => handleLike(question.id)}
          />
        </article>
      ))}
      <ContributionForm
        title="Nova pergunta"
        subtitle="Não encontrou o que procurava? Informe sua dúvida no formulário abaixo:"
        submitCallback={handleSubmitNewQuestion}
      />
    </div>
  )
}

export default Homepage
