import React, { useState, useEffect } from 'react'
import { Link } from 'react-router-dom'
import api from '../../services/api'
import ContributionForm from '../../components/ContributionForm/ContributionForm'
import LikeButton from '../../components/LikeButton/LikeButton'

import './Question.sass'
import '../homepage/Homepage.sass'

const Question = (props) => {
  const [questionState, setQuestionState] = useState({
    questionId: null,
    question: '',
  })
  const [answersState, setAnswersState] = useState({
    answers: [],
  })
  const [sortState, setSortState] = useState('date')

  const loadQuestion = async (id) => {
    const response = await api.get(`questions/${id}?orderBy=${sortState}`)

    setQuestionState(() => ({
      questionId: response.data.id,
      question: response.data.question,
    }))

    setAnswersState(() => ({
      answers: response.data.answers
    }))
  }

  const handleSubmitNewAnswer = async (content) => {
    const { questionId } = questionState
    const response = await api.post('answers', {
      question: questionId,
      answer: content,
    })
    const { answers } = answersState
    answers.push(response.data)
    setAnswersState({ answers })
  }

  const handleLike = async (answerId) => {
    const response = await api.post(`answers/${answerId}/like`)
    const { answers } = answersState

    const newAnswers = answers.map((answer) => (
      answer.id === answerId
        ? response.data
        : answer))

    setAnswersState({ answers: newAnswers })
  }

  const setSort = () => {
    setSortState(sortState === 'date' ? 'likes' : 'date')
  }

  useEffect(() => {
    const { id } = props.match.params
    loadQuestion(id)
  }, [sortState])

  const { questionId, question } = questionState
  const { answers } = answersState
  return (
    <div className="question-details">
      <Link to="/">&lsaquo; Todas as perguntas</Link>
      <h1>Pergunta #{questionId}</h1>
      <button type="button" onClick={setSort}>Ordenado por <i>{sortState}</i></button>
      <div className="question-title">
        { question }
      </div>
      {answers && answers.length === 0 && (
        <p className="no-answers-notice">Nenhuma resposta para esta pergunta ainda</p>
      )}
      {answers && answers.length > 0 && (
        answers.map((answer) => (
          <article key={answer.id}>
            <div className="answer-metadata">Resposta #{answer.id} [{answer.createdAt}]</div>
            <p>{answer.answer}</p>
            <LikeButton
              likes={answer.likes}
              handleClick={() => handleLike(answer.id)}
            />
          </article>
        ))
      )}
      <ContributionForm
        title="Nova resposta"
        subtitle="Contribua com sua resposta:"
        submitCallback={handleSubmitNewAnswer}
      />
    </div>
  )
}
export default Question
