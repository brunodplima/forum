import React from 'react'

import { BrowserRouter, Switch, Route } from 'react-router-dom'

import Homepage from './pages/homepage/Homepage'
import Question from './pages/question/Question'

const Routes = () => (
  <BrowserRouter>
    <Switch>
      <Route exact path="/" component={Homepage} />
      <Route path="/questions/:id" component={Question} />
    </Switch>
  </BrowserRouter>
)

export default Routes
